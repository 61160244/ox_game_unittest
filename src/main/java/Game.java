/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Windows
 */
import java.util.Scanner;

/**
 *
 * @author Windows
 */
public class Game {

    private char Continue = 'Y';
    private Table table;
    private int row, column;
    private Player X, O;

    Game() {
        X = new Player('X');
        O = new Player('O');
        

    }

    public void startGame() {
        table = new Table(X, O);
    }

    public void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    public void showEndTurn() {
        System.out.println("-----------------------");
    }

    public void showInputRowColumn() {
        System.out.print("Enter your row&column : ");
    }

    public void inputRowColumn() {
        Scanner kb = new Scanner(System.in);
        do {
            showInputRowColumn();
            row = kb.nextInt();
            column = kb.nextInt();
        } while (table.checkPosition(row, column));
    }

    public void showSelectPlayer() {
        System.out.println("Select first player");
        System.out.println("    1:X   2:O");
    }

    public void showTable() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(table.getData()[i][j] + " ");
            }
            System.out.println("");
        }
    }

    public void showTurn() {
        System.out.println(table.getPlayer().getName() + " Turn");
    }

    public void showEndGame() {
        System.out.println("Bye bye!!");
    }

    public void showWinner() {
        System.out.println(table.getWinner().getName() + " Win");
    }

    public void inputContinue() {
        Scanner kb = new Scanner(System.in);
        System.out.print("Continue??" + "(Y/N): ");
        Continue = kb.next().charAt(0);
    }

    public void showScore() {
        System.out.println("X Win:" + X.getWin() + " Lost:" + X.getLost()
                + " Draw:" + X.getDraw());
        System.out.println("O Win:" + O.getWin() + " Lost:" + O.getLost()
                + " Draw:" + O.getDraw());
    }

    public void run() {
        Scanner kb = new Scanner(System.in);
        while (Continue == 'Y') {
            startGame();
            showWelcome();
            showSelectPlayer();
            table.selectFirstPlayer(kb.nextInt());
            do {
                showTable();
                showTurn();
                inputRowColumn();
                table.inputRowColumn(row, column);
                showEndTurn();
                table.switchTurn();
            } while (table.checkWin(row, column) == false && table.getCountTurn() < 9);
            showTable();
            if (table.checkWin(row, column)) {
                showWinner();
                table.updateScore();
            }
            showScore();
            inputContinue();
            if (Continue == 'N') {
                showEndGame();
            }
        }
    }
}
