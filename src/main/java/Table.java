/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Windows
 */
public class Table {

    private char[][] data = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    private Player player, O, X, win;
    private int countTurn = 0;

    Table(Player X, Player O) {
        this.X = X;
        this.O = O;
    }

    public void inputRowColumn(int row, int column) {
        data[row - 1][column - 1] = player.getName();
        countTurn++;
    }

    public Player getWinner() {
        return win;
    }

    public void switchTurn() {
        if (player == X) {
            player = O;
        } else {
            player = X;
        }
    }

    public Player getPlayer() {
        return player;
    }

    public char[][] getData() {
        return data;
    }

    private boolean checkWinRow(int row) {
        if (data[row - 1][0] == data[row - 1][1] && data[row - 1][0] != '-') {
            if (data[row - 1][1] == data[row - 1][2]) {
                return true;
            }
        }
        return false;
    }

    private boolean checkWinColumn(int column) {
        if (data[0][column - 1] == data[1][column - 1] && data[0][column - 1] != '-') {
            if (data[1][column - 1] == data[2][column - 1]) {
                return true;
            }
        }
        return false;
    }

    private boolean checkWinDiagonal1() {
        if (data[0][0] == data[1][1] && data[0][0] != '-') {
            if (data[1][1] == data[2][2]) {
                return true;
            }
        }
        return false;
    }

    private boolean checkWinDiagonal2() {
        if (data[0][2] == data[1][1] && data[0][2] != '-') {
            if (data[1][1] == data[2][0]) {
                return true;
            }
        }
        return false;
    }

    public boolean checkWin(int row, int column) {
        if (checkWinRow(row)) {
            switchTurn();
            win = player;
            return true;
        } else if (checkWinColumn(column)) {
            switchTurn();
            win = player;
            return true;
        } else if (checkWinDiagonal1()) {
            switchTurn();
            win = player;
            return true;
        } else if (checkWinDiagonal2()) {
            switchTurn();
            win = player;
            return true;
        } else if (countTurn == 9) {
            countTurn++;
            O.addDraw();
            X.addDraw();
            return false;
        } else {
            return false;
        }
    }

    public boolean checkPosition(int row, int column) {
        if (data[row - 1][column - 1] != '-') {
            System.out.println("No has this position!! Please try again.");
            return true;
        } else if ((row > 3 || column > 3) && (row < 1 || column < 1)) {
            System.out.println("This position isn't empty!! Please try again.");
            return true;
        }
        return false;
    }

    public void selectFirstPlayer(int n) {
        if (n == 1) {
            player = X;
        } else {
            player = O;
        }
    }

    public void updateScore() {
        if (win.getName() == O.getName()) {
            O.addWin();
            X.addLost();
        } else if (win.getName() == X.getName()) {
            X.addWin();
            O.addLost();
        }
    }

    public int getCountTurn() {
        return countTurn;
    }
}

