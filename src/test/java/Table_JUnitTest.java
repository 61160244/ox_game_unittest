/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Windows
 */
public class Table_JUnitTest {

    public Table_JUnitTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testSelectFristPlayer() {
        Player x = new Player('x');
        Player o = new Player('o');
        Table table = new Table(x, o);
        table.selectFirstPlayer(1);
        assertEquals('x', table.getPlayer().getName());
    }

    public void testGetData() {
        Player x = new Player('x');
        Player o = new Player('o');
        Table table = new Table(x, o);
        char[][] data = table.getData();
        assertEquals('-', data[0][0]);
    }

    public void testInputRowColumn() {
        Player x = new Player('x');
        Player o = new Player('o');
        Table table = new Table(x, o);
        table.selectFirstPlayer(1);
        table.inputRowColumn(1, 1);
        char[][] data = table.getData();
        assertEquals('x', data[0][0]);
    }

    public void testGetPlayer() {
        Player x = new Player('x');
        Player o = new Player('o');
        Player player;
        Table table = new Table(x, o);
        table.selectFirstPlayer(2);
        player = table.getPlayer();
        assertEquals('o', player.getName());
    }

    public void testGetCountTurn() {
        Player x = new Player('x');
        Player o = new Player('o');
        Table table = new Table(x, o);
        assertEquals(0, table.getCountTurn());
    }

    public void testSwitchTurn() {
        Player x = new Player('x');
        Player o = new Player('o');
        Table table = new Table(x, o);
        table.selectFirstPlayer(1);
        table.switchTurn();
        assertEquals('o', table.getPlayer().getName());
    }

    public void testCheckWinRow() {
        Player x = new Player('x');
        Player o = new Player('o');
        Table table = new Table(x, o);
        table.selectFirstPlayer(1);
        table.inputRowColumn(1, 1);
        table.inputRowColumn(1, 2);
        table.inputRowColumn(1, 3);
        assertEquals(true, table.checkWin(1, 1));
    }

    public void testCheckWinColumn() {
        Player x = new Player('x');
        Player o = new Player('o');
        Table table = new Table(x, o);
        table.selectFirstPlayer(1);
        table.inputRowColumn(1, 1);
        table.inputRowColumn(2, 1);
        table.inputRowColumn(3, 1);
        assertEquals(true, table.checkWin(1, 1));
    }

    public void testCheckWinDiagonal1() {
        Player x = new Player('x');
        Player o = new Player('o');
        Table table = new Table(x, o);
        table.selectFirstPlayer(1);
        table.inputRowColumn(1, 1);
        table.inputRowColumn(2, 2);
        table.inputRowColumn(3, 3);
        assertEquals(true, table.checkWin(1, 1));
    }
    
    public void testCheckWinDiagonal2() {
        Player x = new Player('x');
        Player o = new Player('o');
        Table table = new Table(x, o);
        table.selectFirstPlayer(1);
        table.inputRowColumn(1, 3);
        table.inputRowColumn(2, 2);
        table.inputRowColumn(3, 1);
        assertEquals(true, table.checkWin(1, 3));
    }

}
