/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Windows
 */
public class Player_JUnitTest {

    public Player_JUnitTest() {
    }

    @org.junit.jupiter.api.BeforeAll
    public static void setUpClass() throws Exception {
    }

    @org.junit.jupiter.api.AfterAll
    public static void tearDownClass() throws Exception {
    }

    @org.junit.jupiter.api.BeforeEach
    public void setUp() throws Exception {
    }

    @org.junit.jupiter.api.AfterEach
    public void tearDown() throws Exception {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testGetName() {
        Player x = new Player('x');
        assertEquals('x', x.getName());
    }

    public void testGetWin() {
        Player x = new Player('x');
        assertEquals(0, x.getWin());
    }

    public void testGetLost() {
        Player x = new Player('x');
        assertEquals(0, x.getLost());
    }

    public void testGetDraw() {
        Player x = new Player('x');
        assertEquals(0, x.getDraw());
    }

    public void testAddWin() {
        Player x = new Player('x');
        x.addWin();
        assertEquals(1, x.getWin());
    }

    public void testAddLost() {
        Player x = new Player('x');
        x.addLost();
        assertEquals(1, x.getLost());
    }

    public void testAddDraw() {
        Player x = new Player('x');
        x.addDraw();
        assertEquals(1, x.getDraw());
    }
}
